AutoLog.views.Viewport = Ext.extend(Ext.Panel, {
    fullscreen: true,
    layout: 'card',

    initComponent: function() {
        Ext.apply(this, {
            items: [
                { xtype: 'AutoLog.views.VehiclesList', id: 'vehiclesList' },
                { xtype: 'AutoLog.views.VehiclesForm', id: 'vehiclesForm' },
            ]
        });
        AutoLog.views.Viewport.superclass.initComponent.apply(this, arguments);
    },

    reveal: function(target) {
        var direction = (target === 'vehiclesList') ? 'right' : 'left'
        this.setActiveItem(
            AutoLog.views[target],
            { type: 'slide', direction: direction }
        );
    }
});
