AutoLog.views.VehiclesForm = Ext.extend(Ext.form.FormPanel, {

    initComponent: function(){
        var titlebar, cancelButton, buttonbar, saveButton, fields, bottomBar, deleteButton;
		
        cancelButton = {
            text: 'Cancel',
            ui: 'back',
            handler: this.onCancelAction,
            scope: this
        };

		saveButton = {
            id: 'vehicleFormSaveButton',
            text: 'Save',
            ui: 'action',
            handler: this.onSaveAction,
            scope: this,

        };

        titlebar = {
            id: 'vehicleFormTitleBar',
            dock: 'top',
			xtype: 'toolbar',
            title: 'Create Vehicle',
            items: [ cancelButton , {xtype: 'spacer'}, saveButton]
        };

		fields = {
			xtype: 'fieldset',
	        id: 'vehicleFormFieldset',
            title: 'Vehicle Details',
            instructions: 'Please enter the information above.',
            defaults: {
                xtype: 'textfield',
                labelAlign: 'left',
                labelWidth: '40%',
                required: false,
                useClearIcon: true,
                autoCapitalize : false
            },
            items: [
                {
                    
					name : 'make',
                    label: 'Make',
					required: true,
					autoCapitalize: true
                },
                {
                    name: 'model',
                    label: 'Model',
					autoCapitalize: true,
					required: true,
                },
                {
                    
					name: 'year',
                    label: 'Year',
					value: '2011'
                },
            ]
		};
		
		deleteButton = {
			text: 'Delete',
			ui: 'decline',
			handler: this.onDeleteAction,
			scope: this
		};
	
		bottomBar = {
			id: 'vehicleFormBottomBar',
			dock: 'bottom',
			xtype: 'toolbar',
			items: [{xtype: 'spacer'}, deleteButton]
		};
		
	


       Ext.apply(this, {
            scroll: 'vertical',
            dockedItems: [ titlebar, bottomBar ],
			items: [fields],
			listeners: {
				beforeActivate: function(){
					var saveButton = this.down('#vehicleFormSaveButton'),
					    titleBar = this.down('#vehicleFormTitleBar'),
						bottomBar = this.down('#vehicleFormBottomBar'),
					    model = this.getRecord();
					
					if (model.phantom){
						titleBar.setTitle('Create Vehicle');
						bottomBar.hide();
					}
					else{
						titleBar.setTitle('Update Vehicle');
					}
				}
			}
        });

        AutoLog.views.VehiclesForm.superclass.initComponent.call(this);
    },

    onCancelAction: function() {
        Ext.dispatch({
            controller: 'Vehicles',
            action: 'index'
        });
    },

	onDeleteAction: function(){
		//add confirmation sheet here.
		this.onDeleteConfirmAction();
	},
	
	onDeleteConfirmAction: function(){
		Ext.dispatch({
			controller: 'Vehicles',
			action: 'delete',
			data: this.getValues(),
			record: this.getRecord()
		})
	},

    onSaveAction: function() {
		Ext.dispatch({
			controller: 'Vehicles',
			action: 'save',
			data: this.getValues(),
			record: this.getRecord()
		});
    }

});

Ext.reg('AutoLog.views.VehiclesForm', AutoLog.views.VehiclesForm);