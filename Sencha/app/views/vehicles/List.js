AutoLog.views.VehiclesList = Ext.extend(Ext.Panel, {
    initComponent: function(){
        var addButton, titlebar, list;

        addButton = {
            itemId: 'addButton',
            text: 'Add Vehicle',
            ui: 'normal',
            handler: this.onAddAction,
            scope: this
        };

        titlebar = {
            dock: 'top',
            xtype: 'toolbar',
            title: 'AutoLog',
            items: [ { xtype: 'spacer' }, addButton ]
        };

        list = {
            xtype: 'list',
            itemTpl: '{year} {make} {model}',
            store: AutoLog.stores.vehicles,
			listeners: {
				scope: this,
				itemTap: this.onItemTapAction
			}
        };

        Ext.apply(this, {
            html: 'placeholder',
            layout: 'fit',
            dockedItems: [titlebar],
            items: [list]
        });

        AutoLog.views.VehiclesList.superclass.initComponent.call(this);
    },

    onAddAction: function() {
 		Ext.dispatch({
            controller: 'Vehicles',
            action: 'newForm'
        });
    },

	onItemTapAction: function(list, index, item, e){
		Ext.dispatch({
			controller: 'Vehicles',
			action: 'editForm',
			index: index
		});
	}

});

Ext.reg('AutoLog.views.VehiclesList', AutoLog.views.VehiclesList);