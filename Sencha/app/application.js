new Ext.Application({
    name: 'AutoLog',

    launch: function() {
        this.views.viewport = new this.views.Viewport();

		this.views.vehiclesList = this.views.viewport.down('#vehiclesList');
        this.views.vehiclesForm = this.views.viewport.down('#vehiclesForm');
   }
});