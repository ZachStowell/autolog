Ext.regController('Vehicles', {
	store: AutoLog.stores.vehicles,

    index: function() {
        AutoLog.views.viewport.reveal('vehiclesList');
    },

    newForm: function() {
    	var model = new AutoLog.models.Vehicle();
		AutoLog.views.vehiclesForm.load(model);
    	AutoLog.views.viewport.reveal('vehiclesForm');
    },

	editForm: function(params){
		var model = this.store.getAt(params.index);
		AutoLog.views.vehiclesForm.load(model);
		AutoLog.views.viewport.reveal('vehiclesForm');
	},
	
	update: function(params){
		params.record.set(params.data);
		params.record.save();
		this.index();
	},
	
	delete: function(params){
		console.log(params);
		this.store.remove(params.record);
		this.store.sync();
		this.index();
	},

	save: function(params){
		params.record.set(params.data);
		this.store.create(params.data);
		this.index();
	}

});