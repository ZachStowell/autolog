Ext.regModel('Fuel',{
	fields: [
		'id',
		'vehicle_id',
		{name: 'odometer', type: 'float'},
		{name: 'pricePerGallon', type: 'float'},
		{name: 'gallons', type: 'float'},
	],
	
	calculateCost: function(){
		var ppg = this.get('pricePerGallon');
		var gal = this.get('gallons');
		
		return ppg*gal;		
	},
	
	belongsTo: 'Vehicle'
});