AutoLog.models.Vehicle = Ext.regModel('Vehicle',{
	fields: [
		'id',
		{name: 'year', type: 'int'},
		{name: 'make', type: 'string'},
		{name: 'model', type: 'string'},
	],
	
	proxy: {
		type: 'localstorage',
		id: 'autoLog-vehicles'
	}
});

