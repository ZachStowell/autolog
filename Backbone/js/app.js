$(function(){
	window.Vehicle = Backbone.Model.extend({
		defaults: {
			"make": "",
			"model": "",
			"year": "",
		}
	});

	window.Record = Backbone.Model.extend({
		defaults: {
			"odometer": "",
			"cost": "",
			"type": "",
			"location": {"lat": 0, "long": 0},
		}
	})

	window.FuelEntry = Record.extend({
		defaults: {
			"unit price": "",
			"amount": "",
			"unit": "gallons",
		}
	});

	window.VehicleList = Backbone.Collection.extend({
		model: Vehicle
	});

	window.vehicleList = new Backbone.Collection([
			{make: "Hyundai", model: "Genesis Coupe", year: "2010"}, 
			{make: "Nissan", model: "350z", year: "2005"}
			]);

	window.ServiceEntry = Record.extend({
		defaults: {
			"description": "",
		}
	});

	window.AppView = Backbone.View.extend({
		el: $('#autoLog'),


	});

	window.App = new AppView;
});